//
//  ViewController.m
//  RecordingAndPlaying
//
//  Created by James Cash on 27-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@property (nonatomic, strong) NSURL *recordingURL;
@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, strong) AVAudioPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Get our app's private document direction to save stuff into
    NSURL *documentDir = [[[NSFileManager defaultManager]
                           URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]
                          firstObject];
    // Make a URL to a m4a file inside our document directory
    self.recordingURL = [documentDir URLByAppendingPathComponent:@"recording.m4a"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecording:(UIButton *)sender {
    // If we've already started recording, pressing this button again will stop it
    // note that we take advantage of the fact that if self.recorder is nil, any methods on it return nil, and nil is considered false
    // therefore, if we haven't started recording, this will be false
    if (self.recorder.isRecording) {
        // if we're recording, pressing button stops & resets the button title
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        return;
    }

    // otherwise, change button title
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err;
    // create the recorder...
    self.recorder = [[AVAudioRecorder alloc]
                     // to record to our file in our docs directory
                     initWithURL:self.recordingURL
                     // and tell it to record as m4a, with some reasonable parameters
                     settings:@{AVSampleRateKey: @(44100),
                                AVNumberOfChannelsKey: @(2),
                                AVFormatIDKey: @(kAudioFormatMPEG4AAC)}
                     error:&err];

    // audio session represents the shared use of the audio subsystem of the device
    // we need to say what we intend to do with it
    [[AVAudioSession sharedInstance]
     setCategory:AVAudioSessionCategoryPlayAndRecord error:&err];

    // start recording
    [self.recorder record];
}

- (IBAction)togglePlayback:(UIButton *)sender {
    // same idea as in record button; stop playing & reset title if already playing...
    if (self.player.isPlaying) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        return;
    }

    // otherwise, start playing & change button title
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err;
    // create a player to play from our URL that we saved the recording to
    self.player = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:self.recordingURL
                   error:&err];
    // and play it
    [self.player play];
}

@end
